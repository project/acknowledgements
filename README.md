## CONTENTS OF THIS FILE

 * Introduction
 * Requirements
 * Recommended Modules
 * Installation
 * Configuration
 * Maintainers


## INTRODUCTION

The Sign for acknowledgement module is useful if you have to check which users
have read your Drupal documents. When these documents are displayed, a checkbox
appears and the current user should be able to sign for acknowledgement by
selecting it.

 * For a full description of the module visit:
   https://www.drupal.org/project/acknowledgements

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/acknowledgements


## REQUIREMENTS

This module requires the following module:

 * Token - https://www.drupal.org/project/token


## RECOMMENDED MODULES

 * Multiple Selects - https://www.drupal.org/project/multiple_selects


## INSTALLATION

Install the Sign for acknowledgement module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


## CONFIGURATION

    1. Navigate to Administration > Extend and enable the Sign for
       acknowledgement module and its dependencies.
    2. Navigate to Administration > Configuration > People > Acknowledgement
       to configure.
    3. Select the node types to be handled by the module.
    4. Choose whether to show a message when a node has no acknowledgement.
    5. Select the user and roles to support by default.
    6. Select the layout for display.
    7. Configure the email notifications.
    8. Save configurations.


## MAINTAINERS

 * Paolo Bozzo (pagolo) - https://www.drupal.org/u/pagolo
